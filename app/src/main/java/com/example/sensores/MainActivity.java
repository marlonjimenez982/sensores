package com.example.sensores;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    //paso 1
    SensorManager miSensorManager;

    //paso 2 crear un objeto por cada sensor
    Sensor miSensor;
    TextView txtAceleracion;

    int puntos=0;
    GraphView grafica;
    LineGraphSeries <DataPoint> series;

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //conexion con la otra clase de Menu
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        txtAceleracion = findViewById(R.id.txtAceleracion);

        grafica =findViewById(R.id.grafico);
        grafica.getViewport().setXAxisBoundsManual(true);

        series= new LineGraphSeries<DataPoint>(new DataPoint[]{});

        //Agregar series a mi grafico
        grafica.addSeries(series);

        //Cambiar el color de las series
        series.setColor(getResources().getColor(R.color.purple_500));

        //paso 3
        miSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //Saber los sensores
        //List<Sensor> misSensores = miSensorManager.getSensorList(Sensor.TYPE_ALL);

        //Paso 4 llamamos sensor Acelerometro
        miSensor=miSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        //Registrar el listener
        miSensorManager.registerListener(this, miSensor, SensorManager.SENSOR_DELAY_NORMAL);

    }

    //Pasos 5 implementar los metodos
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {

        int x= (int) sensorEvent.values[0];
        int y= (int) sensorEvent.values[1];
        int z= (int) sensorEvent.values[2];

        //prueba
        //txtAceleracion.setText("se movio");

        //txtAceleracion.setText("X: "+x+" - Y: "+y+" - Z:"+z);

        //paso 6 Calcular distancia entre puntos
        double aceleracionTotal;
        aceleracionTotal=Math.sqrt(Math.pow(x,2) + Math.pow(y,2) + Math.pow(z,2) );
        txtAceleracion.setText("Acelelracion "+aceleracionTotal);

        //mostrar un punto en el grafico
        DataPoint mipunto;
        mipunto=new DataPoint(series.getHighestValueX()+1, aceleracionTotal);
        puntos++;

        series.appendData(mipunto, true, puntos);

        grafica.getViewport().setMinX(puntos-150);
        grafica.getViewport().setMaxX(puntos);

        if (puntos>500){
            puntos=0;
            series.resetData(new DataPoint[]{});
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    //Activar el menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflador=getMenuInflater();
        inflador.inflate(R.menu.main_menu,menu);
        return true;
    }

}